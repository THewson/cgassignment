﻿Shader "Custom/RetroPixelArt" {
	Properties{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_PixelSize("Pixel Size", Range(0.001, 0.1)) = 1.0 // size of each pixel. 0.001 minimum prevents screen blanking
	}
	SubShader{
		Tags { "RenderType" = "Opaque" }

	Pass {
		CGPROGRAM

		#pragma vertex vert_img 
		#pragma	fragment frag
		#include "UnityCG.cginc"

		sampler2D _MainTex;
		float _PixelSize;

		fixed4 frag(v2f_img i) : SV_Target{
			fixed4 col;

			// Clamps pixel size
			float ratioX = (int)(i.uv.x / _PixelSize) * _PixelSize;
			float ratioY = (int)(i.uv.y / _PixelSize) * _PixelSize;

			col = tex2D(_MainTex, float2(ratioX, ratioY));
			
			// Greyscales the image
			col = dot(col.rgb, float3(0.3, 0.59, 0.11));

			if (col.r <= 0.4) {
				col = fixed4(0.164705882, 0.168627450, 0.164705882, 1.0);
			}
			else if (col.r > 0.4 && col.r <= 0.5) {
				col = fixed4(0.36862745098, 0.28627450926, 0.3333333327, 1.0);
			}
			else if (col.r > 0.5 && col.r < 0.75) {
				col = fixed4(0.59999999886, 0.40784313648, 0.53333333232, 1.0);
			}
			else if (col.r > 0.75 && col.r < 0.85) {
				col = fixed4(0.78823529262, 0.61568627334, 0.63921568506, 1.0);
			}
			else {
				col = fixed4(0.77647058676, 0.86666666502, 0.9411764688, 1.0);
			}
			
			
			//else if (col.r > 0.25 && col.r <= 0.5) {
			//	col = fixed4(0.19, 0.38, 0.19, 1.0);
			//}
			//else {
			//	col = fixed4(0.54, 0.67, 0.06, 1.0);
			//}
			return col;
		}

		ENDCG
		}
	}
}