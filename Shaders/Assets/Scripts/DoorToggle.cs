﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorToggle : MonoBehaviour {

    public GameObject door;
    
	public void OnClick()
    {
        door.SetActive(false);
    }
}
