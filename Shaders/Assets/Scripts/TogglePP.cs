﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TogglePP : MonoBehaviour {
    public PostProcessing pp;

    public void OnClick()
    {
        pp.enabled = !pp.enabled;
    }
}
