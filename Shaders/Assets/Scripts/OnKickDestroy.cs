﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnKickDestroy : MonoBehaviour {

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (other.gameObject.GetComponent<Animator>().GetBool("Kicking"))
            {
                Destroy(this.gameObject, 1);
            }
        }
    }
    
}
