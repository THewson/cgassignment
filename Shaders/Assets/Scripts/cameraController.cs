﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;
    public Vector3 offset;
    public Vector3 intOffset;

	// Use this for initialization
	void Start () {
        offset = player.transform.position - transform.position + intOffset;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = player.transform.position - offset;

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
	}
}
