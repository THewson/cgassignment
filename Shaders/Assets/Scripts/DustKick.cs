﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DustKick : MonoBehaviour {
    public GameObject prefab;

    void Footstep() {
        GameObject go = Instantiate(prefab);
        go.transform.position = transform.position + new Vector3(0, 0.2f, 0);
        Destroy(go, 2);
    }
}
