﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MartialArt : MonoBehaviour {

    Animator animator;

	// Use this for initialization
	void Start () {
        animator = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Q))
        {
            animator.SetBool("Kicking", true);
        }
        else
        {
            animator.SetBool("Kicking", false);
        }
	}
}
